package com.oleksandryerm.fastorder.configurations;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = { "com.oleksandryerm.fastorder"})
@PropertySource("classpath:server.properties")
public class ServerConfiguration {
}
