package com.oleksandryerm.fastorder;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            logger.info("Starting application");
            new EmbeddedTomcat().start();
        } catch (IOException e) {
            logger.error("Could not start embedded tomcat");
            e.printStackTrace();
        }
    }

}
